const express = require('express')
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const session = require('express-session');
const path = require('path') ;
const ideas = require('./routes/ideas');
const users = require('./routes/users');

const app = express()


// connect database
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/vidjot-dev', { useNewUrlParser: true })
    .then(() => console.log('MongoDB is connected'))
    .catch(err => console.log(err));

app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// static folder
app.use(express.static(path.join(__dirname, 'public'))) ;

// methodoverride middlware
app.use(methodOverride('_method'));

// Express session midleware
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

app.use(flash());
// Global variables
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});

app.use('/ideas', ideas);
app.use('/users', users);

app.get('/', (req, res) => {
    const title = 'welcome';
    res.render('index', { title: title });
});

app.get('/about', (req, res) => {
    res.render('about');
});


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`port is listning on ${PORT}`);
})