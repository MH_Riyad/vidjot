const router = require('express').Router()
const Idea = require('../models/Idea')


// add idea form
router.get('/add', (req, res) => {
    res.render('ideas/add');
});
// idea post 
router.post('/', (req, res) => {
    let errors = [];
    let { title, details } = req.body;

    if (!title || !details) {
        errors.push({ text: 'Please add all fields' })
    }
    if (errors.length > 0) {
        res.render('ideas/add', {
            errors: errors,
            title: req.body.title,
            details: req.body.details
        });
    } else {
        const newUser = {
            title: req.body.title,
            details: req.body.details
        }
        new Idea(newUser)
            .save()
            .then(idea => {
                req.flash('success_msg', 'video idea added');
                res.redirect('/ideas');
            })
    }
});

// Idea Index page
router.get('/', (req, res) => {
    Idea.find({})
        .then(ideas => {
            res.render('ideas/index', { ideas: ideas });
        })
        .catch(err => console.log(err));

})
// edit ideas 
router.get('/edit/:id', (req, res) => {
    Idea.findOne({
        _id: req.params.id
    }).then(idea => {
        res.render('ideas/edit', { idea: idea });
    });
})

// put ideas
router.put('/:id', (req, res) => {
    Idea.findOne({
        _id: req.params.id
    }).then(idea => {
        idea.title = req.body.title,
        idea.details = req.body.details

        idea.save().
            then(idea => {
                req.flash('success_msg', 'video idea updated');
                res.redirect('/ideas')
            });

    });
});

// delete idea
router.delete('/:id', (req, res) => {
    Idea.remove({ _id: req.params.id }).
        then(() => {
            req.flash('success_msg', 'video idea removed');
            res.redirect('/ideas');
        });
});

module.exports = router;