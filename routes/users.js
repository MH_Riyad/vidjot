const router = require('express').Router();
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const User = require('../models/User')

// login page
router.get('/login', (req, res) => {
    res.render('users/login');
});

// register page
router.get('/register', (req, res) => {
    res.render('users/register');
});

// after filling all fields in register form
router.post('/register', (req, res) => {
    let errors = [];
    if (req.body.password != req.body.password2) {
        errors.push({ text: 'password do not match' });
    }
    if (req.body.password.length < 4) {
        errors.push({ text: 'password must at least   4 length' })
    }
    if (errors.length > 0) {
        res.render('users/register', {
            errors: errors,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            password2: req.body.password2
        });
    } else {
        User.findOne({ email: req.body.email })
            .then(user => {
                if (user) {
                    req.flash('error_msg', 'email already registered');
                    res.redirect("/users/login");
                } else {
                    const newUser = new User({
                        name: req.body.name,
                        email: req.body.email,
                        password: req.body.password
                    });
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(newUser.password, salt, (err, hash) => {
                            if (err) throw err;
                            newUser.password = hash;
                            newUser.save()
                                .then(user => {
                                    req.flash("success_msg", "you are now registered");
                                    res.redirect("/users/login");
                                }).catch(err => {
                                    console.log(err);
                                    return;
                                });
                        })
                    });
                }
            })



    }
});

module.exports = router;